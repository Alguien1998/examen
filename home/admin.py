from django.contrib import admin

# Register your models here.
from .models import Jugador

admin.site.register(Jugador)

from .models import Equipo

admin.site.register(Equipo)

from .models import Estadio

admin.site.register(Estadio)